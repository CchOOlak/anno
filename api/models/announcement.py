from django.db import models

from api.models import User


class Announcement(models.Model):
    title = models.CharField(max_length=250, blank=True)
    content = models.TextField(null=True, blank=True)
    keywords = models.TextField(null=True, blank=True)
    author = models.ForeignKey(
        User, related_name='announcements', on_delete=models.CASCADE)
    views = models.IntegerField(default=0)

    status_enum = (
        (0, 'pending'),
        (1, 'accepted'),
        (2, 'rejected'),
    )
    status = models.IntegerField(choices=status_enum, default=0)

    created_at = models.DateField(auto_now=True)

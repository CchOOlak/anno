from rest_framework import serializers

from api.models import Announcement
from api.serializers import UserProfileSerializer


class AnnouncementSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(max_length=250, required=False)
    content = serializers.CharField(required=False)
    keywords = serializers.CharField(required=False)
    author = UserProfileSerializer(read_only=True)
    views = serializers.IntegerField(read_only=True)
    status = serializers.ChoiceField(
        choices=Announcement.status_enum, read_only=True)
    created_at = serializers.DateField(read_only=True)

    class Meta:
        model = Announcement
        fields = ('id', 'title', 'content', 'keywords',
                  'author', 'views', 'status', 'created_at')

    def create(self, validated_data):
        author = self.context['request'].user
        return Announcement.objects.create(
            title=validated_data['title'],
            content=validated_data['content'],
            keywords=validated_data['keywords'],
            author=author,
        )

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.content = validated_data.get('description', instance.content)
        instance.keywords = validated_data.get('keywords', instance.keywords)
        instance.save()
        return instance

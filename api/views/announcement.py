from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action, api_view, permission_classes

from api.models import Announcement
from api.serializers import AnnouncementSerializer
from api.permissions import IsOwnerPermission


class AnnouncementViewSet(viewsets.ModelViewSet):
    queryset = Announcement.objects.all()
    permission_classes = (IsOwnerPermission, )
    serializer_class = AnnouncementSerializer

    @action(methods=['POST'], detail=True, permission_classes=[permissions.IsAdminUser], url_path='accept')
    def announcement_accept_api(self, request, pk=None):
        try:
            anno = Announcement.objects.get(pk=pk)
            anno.status = 1
            anno.save()
            return Response({
                "message": "announcement accepted successfully"
            }, 200)
        except Exception as e:
            return Response({
                "message": "announcement not found"
            }, status=404)

    @action(methods=['GET'], detail=False, permission_classes=[permissions.AllowAny], url_path='search')
    def announcement_search_api(self, request):
        query = request.GET.get('query', "")
        result = Announcement.objects.filter(title__contains=query)
        serialized = AnnouncementSerializer(result, many=True).data
        return Response(serialized)

    @action(methods=['GET'], detail=True, permission_classes=[permissions.AllowAny], url_path='views')
    def announcement_views_count_api(self, request, pk=None):
        try:
            anno = Announcement.objects.get(pk=pk)
            return Response({
                "view_count": anno.views
            }, 200)
        except Exception as e:
            return Response({
                "message": "announcement not found"
            }, status=404)


@api_view(["GET"])
@permission_classes([permissions.IsAuthenticated])
def get_my_announcements(request):
    result = Announcement.objects.filter(author=request.user)
    serialized = AnnouncementSerializer(result, many=True).data
    return Response(serialized)

from django.urls import path

from rest_framework import routers

from api.views import UserRegisterView, UserLoginView, get_profile,\
    AnnouncementViewSet, get_my_announcements


router = routers.SimpleRouter()
router.register(r'announcements', AnnouncementViewSet)

urlpatterns = [
    # Authentication
    path('sign-up/', UserRegisterView.as_view(), name='register'),
    path('login/', UserLoginView.as_view(), name='login'),

    path('profile/', get_profile, name='get_profile'),
    path('my-announcements/', get_my_announcements, name='get_my_announcements'),
]

urlpatterns += router.urls
